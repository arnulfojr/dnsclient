import socket
from SocketContainer.Exceptions.SocketException import SocketException
from SocketStrategy.SocketStrategy import SocketStrategy


class Container(object):
    def __init__(self, sock=None, server=False, tcp=True, debug=True):
        self.server = server
        self.socket = sock
        self.strategy = None
        self.debug = debug
        self.__socket_type = socket.SOCK_STREAM
        if not tcp:
            self.__socket_type = socket.SOCK_DGRAM

    def get_socket(self):
        return self.socket

    def __create_socket(self):
        self.socket = socket.socket(socket.AF_INET, self.__socket_type)
        if self.debug:
            print "Container: Socket created"
        return self.socket

    def set_up(self, address=None, port=None):
        if self.socket:
            raise SocketException("Socket was set up already")
        self.socket = self.__create_socket()
        self.strategy = SocketStrategy(self.socket).get_strategy(server=self.server, protocol=self.__socket_type)
        # gets the correct socket configuration
        self.socket = self.strategy.configure_socket(address=address, port=port)

    def open(self):
        self.strategy.open()
        if self.debug:
            print "Container: Socket is open"

    def close(self):
        self.strategy.close()
        if self.debug:
            print "Container: Socket is closed"
