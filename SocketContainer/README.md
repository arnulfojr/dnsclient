Deprecated Documentation
========================

Task 2 - SMTP Client
--------------------

- Author: Arnulfo Solis.
- Student_ID: 3014693.
- Course: ISE/CE/PO08.
- Lecture: Internet Technology and Web Engineering.
- Python version: 2.7.10

Remarks:
--------
- Definitions of the sockets follow the Socket API.
- The ```SocketContainer``` Package is the same package developed for the Ubung 0. With some minor updates.
    - The class ```SocketIO``` in the ```SocketContainer``` Package added an option to NOT print only when told.
- The ```SMTP```Package contains all the relevant code for this Ubung.
    + Also the package has dependency in ```SocketContainer```.
- The ```SMTP.SenderAgent.EmailSenderAgent``` follows the SMTP Protocol defined in the Lecture slides, as well as the
SMTP codes used by the provided server, for further information read the class ```SMTP.SenderAgent.SMTPCodes```.
- The Agent ```EmailSenderAgent``` falls-back automatically to the ```HELO``` SMTP method if
the ```EHLO``` is not supported.
- Emails are sent with US-ASCII encoding, using Quoted-Printable, respective MIME headers are implemented.

File descriptions:
------------------
- ```Client```: Contains all the surface of the application.
- ```Connection```: Contains everything regarding the wrapping of the socket/connection.
- ```Email```: Wrapper of the elements required for sending an email.
- ```Exceptions```: Exceptions to be able to handle them properly and avoid crashes.
- ```IO```: Handles the printing of the Application
- ```SenderAgent```: Handles the SMTP Protocol for it to send it through the TCP socket.

How to run:
-----------
- Run the ```SMTP``` Package.
    + Exactly: run the ```__init__.py``` file inside the ```SMTP``` package.
- Then enter the email user you want to use.
    + I.E. ```arnulfo@kuzzy.com```.
    + For the ```HELO``` and ```EHLO``` SMTP command, it is defined, as default, the ```qcharts.myarny.org``` client
    identity server, which is pointing to my server.
- Then the client automatically will establish a TCP connection to the default server.
    + The default server is the one provided in the Ubungsblatt.
- If succeeds you'll be prompt to enter the command.
    + ```create email```: As expected, the client will guide you through the process of creating an email.
    + ```list emails```: Will list all the emails residing in the Client (locally).
    + ```send emails```: Will send all emails to the server.
    + ```clear```: Will clear all the local emails.
    + ```end```: Will terminate the Client application.
- The way this client works is that you create a bunch of emails, and it wont send them immediately, it will wait for
the command ```send emails``` to be called and will send all emails to the server.
    + In case of failure, the emails will reside in the list until you execute ```clear```, which will delete them, or
    by calling ```send emails``` again, in wait for a successful server response.
- By calling the command ```end```, the client automatically terminates the SMTP communication, following the protocol,
 and follows to terminate the socket communication, in this case the TCP communication.

Bibliography:
-------------
- https://docs.python.org/2/library/socket.html
- Lecture Slides from SS16 - Internet Technology and Web Engineering.
