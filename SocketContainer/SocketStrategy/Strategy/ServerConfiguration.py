from SocketConfiguration import SocketConfiguration
from SocketContainer.Exceptions.SocketException import SocketException


class TCPServerConfiguration(SocketConfiguration):

    def configure_socket(self, address=None, port=None):
        # configure socket
        try:
            if address:
                self.address = address
            if port:
                self.port = port
            self.socket.bind((self.address, self.port))
        except Exception, e:
            raise SocketException("Error while setting the server's socket", e)
        return self.socket

    def open(self):
        try:
            self.socket.listen(SocketConfiguration.max_connections)
        except Exception, e:
            raise SocketException("Failed to listen to port", e)

    def close(self):
        try:
            self.socket.close()
        except Exception, e:
            raise SocketException("The socket failed to close", e)


class UDPServerConfiguration(TCPServerConfiguration):
    def __init__(self, socket):
        TCPServerConfiguration.__init__(self, socket)

    def open(self):  # todo: change the implementation
        return self.socket  # actually socket is actually binded
