from SocketConfiguration import SocketConfiguration
from SocketContainer.Exceptions.SocketException import SocketException


class ClientConfiguration(SocketConfiguration):

    def configure_socket(self, address=None, port=None):
        if address:
            self.address = address
        if port:
            self.port = port
        return self.socket

    def open(self):
        try:
            self.socket.connect((self.address, self.port))
            print "Client Socket: Connected to %s:%s" % (self.address, self.port)
        except Exception, e:
            message = "\tThe socket failed to connect to %s:%s" % (self.address, self.port)
            raise SocketException(message, e)

    def close(self):
        self.socket.close()
