from SocketContainer.Exceptions.SocketException import SocketException


class SocketConfiguration(object):

    default_address = 'debby.vs.uni-due.de'
    default_port = 2223
    max_connections = 5
    max_default_data_length = 1024

    def __init__(self, sock):
        self.socket = sock
        self.address = SocketConfiguration.default_address
        self.port = SocketConfiguration.default_port

    # return the socket as it is
    def get_socket(self):
        return self.socket

    # override this method!
    def configure_socket(self, address=None, port=None):
        raise SocketException("Socket is not configured")

    def open(self):
        raise SocketException("Method has to be overwritten")

    def close(self):
        raise SocketException("Method has to be overwritten")
