from Strategy.ServerConfiguration import TCPServerConfiguration, UDPServerConfiguration
from Strategy.ClientConfiguration import ClientConfiguration
import socket


class SocketStrategy(object):
    def __init__(self, sock):
        self.socket = sock

    # return a SocketConfiguration instance
    def get_strategy(self, server, protocol=1):
        # if flag is true then is server
        if server:
            # Server configuration
            if protocol == socket.SOCK_STREAM:
                return TCPServerConfiguration(self.socket)
            return UDPServerConfiguration(self.socket)
        # Client Configuration
        return ClientConfiguration(self.socket)
