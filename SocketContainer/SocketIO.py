from SocketContainer.SocketStrategy.Strategy.SocketConfiguration import SocketConfiguration


class SocketIO:

    def __init__(self, sock, origin_address=None, debug=True):
        self.socket = sock
        self.origin = origin_address
        self.debug = debug

    def write(self, message):
        self.socket.sendall(message)

    def read(self, max_length=0):
        if max_length == 0:
            max_length = SocketConfiguration.max_default_data_length
        request = self.socket.recv(max_length)
        if self.debug:
            print "\tSocketIO %s: Read->'%s'" % (str(self.origin), request)
        return request

    def read_as_bytes(self, max_length=1024):  # read by default 1KB
        if max_length == 0:
            max_length = SocketConfiguration.max_default_data_length
        message = bytearray()
        piece = self.socket.recv(max_length)
        message.extend(piece)
        return message

    def close(self):
        if self.debug:
            print "SocketIO %s: connected socket will close" % str(self.origin)
        if self.socket:
            self.socket.close()
        if self.debug:
            print "SocketIO %s: connected socket closed" % str(self.origin)
