class SocketException(Exception):
    def __init__(self, message=None, exception=None):
        super(Exception, self).__init__(message, exception)
        self.message = message
        self.previous_exception = exception

    def __str__(self):
        return self.message

    def get_message(self):
        if self.message is None:
            self.message = "An exception happened related to a Socket"
        return self.message

    def print_trace(self, exception=None):
        print "SocketException: %s" % self.message
        if self.previous_exception is not None:
            print "\tReason: %s" % self.previous_exception
