DNSClient
=========

Task 4 - DNS Client
-------------------
+ Author: Arnulfo Solis
+ Student ID: 3014693
+ Course: ISE CE PO08
+ Lecture: Internet Technology and Web Engineering.
+ Python version: ```2.7.6```

Remarks
-------
+ Definitions of the sockets follow the Socket API.
+ The ```SocketContainer``` Package is the same package developed for
the Ubung 2 and hence also used in the Ubung 0.
    - Some updates done in the ```SocketContainer``` because we will
    handle UDP and TCP protocols, previously only used TCP protocol,
    hence the type of socket created was always using TCP protocol,
    but now the class has been developed to adjust to the desired
    protocol.
        + ```SocketContainer``` now has a full debug mode.
        + ```SocketContainer```

Task
----
+ Create a DNS Client needs to output the A record of the requested
domain name from the previously assigned name server.
+ The Client asks the name server ```dns1.vs.uni-due.de``` name server.
+ And get the A records for the ```debby.vs.uni-due.de``` domain name.
+ DNSClient application extends to work with dynamic name servers, so
the user can choose to which DNS name server to request the information
from.

How to run
----------
+ To run the DNS Client the only thing you need is run the ```DNS```
package.
    - In more detailed words, run the ```/DNS/__init__.py``` file found 
    inside the root folder.
+ Do NOT forget to have the ```SocketContainer``` Package in the root
folder.
    - Inside the ```DNSClient```.