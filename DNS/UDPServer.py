from SocketContainer.Container import Container
from SocketContainer.SocketIO import SocketIO
from binascii import hexlify

__author__ = "asolis"

# test the DNS server coming from the SocketContainer
# this is not used in the ubung at all, was just to test the simple implementation of the udp
# server retrieved from the SocketContainer Package
try:
    server_container = Container(server=True, tcp=False)
    server_container.set_up("localhost", 5001)
    server_container.open()
    io = SocketIO(server_container.get_socket(), origin_address=server_container, debug=True)
    command = io.read_as_bytes()
    print command
    io.close()
except Exception as e:
    print e
