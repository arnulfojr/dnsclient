from SocketContainer.Container import Container
from SocketContainer.Exceptions.SocketException import SocketException


class Connector:  # This should be part of the SocketContainer package
    def __init__(self, address, port, server=False, tcp=True):
        self.port = port
        self.address = address
        self.__socket = None
        self.socket_container = Container(server=server, tcp=tcp)

    def get_socket(self):
        return self.__socket

    def connect(self):
        try:
            self.socket_container.set_up(self.address, self.port)
            self.socket_container.open()
            self.__socket = self.socket_container.get_socket()
        except SocketException as e:
            raise e


class ServerConfiguration:

    default_address = "dns1.vs.uni-due.de"  # fallback to the Uni dns address
    default_ip_address = "134.91.78.133"  # ip from the default address
    default_port = 53  # default to DNS port

    def __init__(self, address=None, port=None):
        self.address = address
        self.port = port

    def get_port(self):
        if not self.port:
            return ServerConfiguration.default_port
        return self.port

    def get_address(self, default_ip=False):
        if not self.address and not default_ip:
            return ServerConfiguration.default_address
        if default_ip:
            return ServerConfiguration.default_ip_address
        return self.address
