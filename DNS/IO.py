class IO:
    def __init__(self):
        return

    @staticmethod
    def log(message, instance=None, tabs=0):
        tabus = ""
        for tabs in range(0, tabs, 1):
            tabus = "\t %s" % tabus
        print "%s%s: %s" % (tabus, instance.__class__.__name__, message)

    @staticmethod
    def ask_command(message):
        return raw_input(message)

    @staticmethod
    def warning(message, instance):
        print "\n[WARNING %s]: %s\n" % (instance.__class__.__name__, message)

    @staticmethod
    def note(message):
        print "\t[NOTE]: %s" % message

    @staticmethod
    def shutdown():
        print "[SHUTDOWN] Bye-bye\nArnulfo Solis, 3014693"
