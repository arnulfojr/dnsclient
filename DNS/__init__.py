from DNS.IO import IO
from DNS.Requests.Content import DNSHeader, DNSQuestion, DNSAnswer
from DNS.Requests.Request import DNSRequest, DNSResponse
from SocketContainer.Container import Container
from SocketContainer.SocketIO import SocketIO
from binascii import hexlify

__author__ = "asolis"


#  test the DNS client coming from the SocketContainer
try:

    name_server = IO.ask_command("Please enter the name server to use:")
    server_container = Container(server=False, tcp=False, debug=False)
    server_container.set_up(name_server, 53)  # Uni name server: 134.91.78.133
    server_container.open()
    io = SocketIO(server_container.get_socket(), debug=False)

    domain_name = IO.ask_command("Please now give me the domain name you want to request:")
    header = DNSHeader()
    content = DNSQuestion(q_name=domain_name)
    request = DNSRequest(header, content)

    io.write(request.get_bits())
    bytes_read = io.read_as_bytes()  # use default max_length
    io.close()

    response = DNSResponse(request_length=len(request.get_bits()))
    response.set_bits(bytes_read)

    IO.note("DNS Response Code: %s" % response.header.get_response_code_description())
    if response.get_answer().r_type == DNSAnswer.A_RECORD:
        IO.log("IP = %s" % response.solve(), response)
    elif response.get_answer().r_type == DNSAnswer.CNAME_RECORD:
        IO.log("\tCNAME (alias) = %s" % response.solve())

    IO.shutdown()

    exit(0)
except Exception as e:
    print e
