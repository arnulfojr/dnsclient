import random


class Generator:

    @staticmethod
    def generate_random_bits(x):
        return random.getrandbits(x)

    @staticmethod
    def get_bin(s): return str(s) if s <= 1 else bin(s >> 1) + str(s & 1)
