from DNS.Requests.Utility import Generator
import binascii


class Bitable(object):

    def get_bits(self):
        raise Exception

    def set_bits(self, bits):
        raise Exception


class DNSHeader(Bitable):

    RESP_NO_ERROR = 0
    RESP_FORMAT_ERROR = 1
    RESP_SERVER_FAIL = 2
    RESP_NAME_ERROR = 3
    RESP_NOT_IMPL = 4
    RESP_REFUSED = 5

    def __init__(self, ref_id=None, qr=0, op_code=0,
                 aa=0, tc=0, r_d=1, r_a=1, z=0, r_c=0,
                 qd_count=1, an_count=0, ns_count=0, ar_count=0):
        Bitable.__init__(self)
        self.unique_id = ref_id  # [16 bits]
        self.qr = qr
        self.op_code = op_code  # 0 for normal query
        self.aa = aa
        self.tc = tc  # Trun-Cation - 1: message was truncated
        self.rd = r_d  # Recursion Desired, 1 is enabled
        self.ra = r_a  # Recursion available: 1
        self.z = z  # for the future!
        self.r_code = r_c  # 4 bit field
        self.qd_count = qd_count  # number of entries in the question section [16 bit]
        self.an_count = an_count  # number of resource records in the answer section [16 bit]
        self.ns_count = ns_count  # number of name server resource rec. in the auth rec. sec. [16 bits]
        self.ar_count = ar_count  # [16 bits] number of resource records in the additional record section
        self.flags = bytearray()
        self.raw = bytearray()
        self._set_up()

    def _set_up(self):
        if not self.unique_id:
            self.unique_id = bytearray([Generator.generate_random_bits(8), Generator.generate_random_bits(8)])

    def get_bits(self):
        bits = bytearray()
        bits.extend(self.unique_id)  # ok
        bits.append(0x01)
        bits.append(0x00)
        bits.append(0x00)
        bits.append(0x01)
        bits.append(0x00)
        bits.append(0x00)
        bits.append(0x00)
        bits.append(0x00)
        bits.append(0x00)
        bits.append(0x00)
        return bits

    def get_response_code_description(self):
        if self.r_code == DNSHeader.RESP_NO_ERROR:
            return "DNS Answer with no error"
        elif self.r_code == DNSHeader.RESP_REFUSED:
            return "DNS refused to answer the question"
        elif self.r_code == DNSHeader.RESP_FORMAT_ERROR:
            return "DNS Question was not properly formatted, name server could not interpret the question"
        elif self.r_code == DNSHeader.RESP_NOT_IMPL:
            return "The DNS name server does not support the type of question"
        elif self.r_code == DNSHeader.RESP_NAME_ERROR:
            return "Authoritative name server: domain referenced in question does not exist"
        else:
            return "DNS Unknown Error"

    def process_flags(self):
        if len(self.flags) <= 0:
            raise AttributeError
        self.r_code = self.flags[1]
        if self.r_code >= 0x80:
            self.ra = True
            self.r_code -= 0x80  # remove the recursion available
        else:
            self.ra = False

    def set_bits(self, resp):
        self.raw = resp
        if isinstance(resp, bytearray):
            self.unique_id = bytearray([resp[:2][0], resp[:2][1]])
            self.flags = resp[2:4]  # TODO: process the flag
            self.qd_count = resp[4:6]
            self.an_count = resp[6:8]
            self.ns_count = resp[8:10]
            self.ar_count = resp[10:12]
            self.process_flags()


class DNSQuestion(Bitable):
    def __init__(self, q_type="A", q_class="IN", q_name=""):
        Bitable.__init__(self)
        self.q_name = q_name
        self.q_type = q_type
        self.q_class = q_class

    @staticmethod
    def get_byte_for_name(domain):
        if not isinstance(domain, str):
            raise TypeError
        zones = domain.split('.')
        result = bytearray()
        for zone in zones:
            result.append(len(zone))
            for bt in zone:
                result.append(bt)
        result.append(0x00)
        return result

    def get_query_type(self):
        if self.q_type == "A":
            return bytearray([0x00, 0x01])
        return bytearray([0x00, 0x00])

    def get_query_class(self):
        if self.q_class == "IN":
            return bytearray([0x00, 0x01])
        return bytearray([0x00, 0x00])

    def get_bits(self):
        b = bytearray()
        b.extend(self.get_byte_for_name(self.q_name))
        b.extend(self.get_query_type())  # hopefully only the A Record code
        b.extend(self.get_query_class())  # hopefully only the IN class
        return b


class DNSAnswer(Bitable):

    A_RECORD = '0001'
    CNAME_RECORD = '0005'

    def __init__(self):
        Bitable.__init__(self)
        self.names = ""
        self.r_type = 0
        self.r_class = ""
        self.ttl = 0
        self.rd_length = 0
        self.r_data = 0
        self.__raw = bytearray()

    def set_bits(self, bits):
        self.__raw = bits
        self.r_type = binascii.hexlify(self.__raw[2:4])
        self.r_class = self.__raw[4:6]

    @staticmethod
    def parse_ip(data):
        result = ""
        for d in data:
            result = "%s.%s" % (result, str(d))
        return result[1:]

    @staticmethod
    def parse_address(data):
        result = ""
        # result = "%s.%s" % (result, str(d).encode("ascii"))
        index = 0
        while data[index] > 0:
            length = data[index]
            loop_range = range(index + 1, length + index + 1)
            if len(data) < length + index:
                result = "%s<__implementation not finished__>" % result  # TODO: finish implementation
                break  # Check the documentation for how to handle this cases
            for subindex in loop_range:
                letter = chr(data[subindex])
                result = "%s%s" % (result, letter)
            result = "%s." % result
            index = length + index + 1
        return result

    def get_ip(self):
        pointer_offset = self.__raw[:2]
        self.r_type = binascii.hexlify(self.__raw[2:4])
        self.r_class = self.__raw[4:6]
        ttl = self.__raw[6:10]
        length = self.__raw[10:12]
        data = self.__raw[12:12 + int(binascii.hexlify(length), 16)]
        if self.r_type == DNSAnswer.A_RECORD:
            return self.parse_ip(data)
        return self.parse_address(data)  # it is an alias

# b.append(0x05)  # length of the piece
# b.append(0x64)
# b.append(0x65)
# b.append(0x62)
# b.append(0x62)
# b.append(0x79)
#
# b.append(0x2)  # length of the piece
# b.append(0x76)
# b.append(0x73)
#
# b.append(0x07)  # length of the piece
# b.append(0x75)
# b.append(0x6e)
# b.append(0x69)
# b.append(0x2d)
# b.append(0x64)
# b.append(0x75)
# b.append(0x65)
#
# b.append(0x02)  # length of the piece
# b.append(0x64)
# b.append(0x65)
#
# # end of name
# b.append(0x00)

# b.append(0x03)
# b.append(0x77)
# b.append(0x77)
# b.append(0x77)
# b.append(0x0c)
# b.append(0x6e)
# b.append(0x6f)
# b.append(0x72)
# b.append(0x74)
# b.append(0x68)
# b.append(0x65)
# b.append(0x61)
# b.append(0x73)
# b.append(0x74)
# b.append(0x65)
# b.append(0x72)
# b.append(0x6e)
# b.append(0x03)
# b.append(0x65)
# b.append(0x64)
# b.append(0x75)
# b.append(0x00)
# b.append(0x00)
# b.append(0x01)
# b.append(0x00)
# b.append(0x01)
