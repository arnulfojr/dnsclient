from DNS.Requests.Content import DNSHeader, DNSQuestion, Bitable, DNSAnswer
import binascii


class DNSRequest(Bitable):
    def __init__(self, headers=None, question=None):
        Bitable.__init__(self)
        self.__headers = headers
        self.__question = question

    def set_headers(self, header):
        if not isinstance(header, DNSHeader):
            raise TypeError
        self.__headers = header
        return self.__headers

    def set_content(self, question):
        if not isinstance(question, DNSQuestion):
            raise TypeError
        self.__question = question
        return self.__question

    def get_bits(self):  # TODO: get the data as binaries
        bits = bytearray()
        bits.extend(self.__headers.get_bits())
        bits.extend(self.__question.get_bits())
        return bits

    def set_bits(self, bits):  # TODO: needed?
        return ""


class DNSResponse(Bitable):

    def __init__(self, request_length=0, bits=None):
        Bitable.__init__(self)
        self.__raw = bits
        self.header = None
        self.answer = None
        self.request_length = request_length

    def get_answer(self):
        return self.answer

    def get_headers(self):
        return self.header

    def set_bits(self, bits):
        self.__raw = bits  # first 12 bytes are header
        self.header = DNSHeader()
        self.answer = DNSAnswer()
        self.header.set_bits(bits[:self.request_length-1])
        self.answer.set_bits(bits[self.request_length:len(bits)])
        return self.__raw

    def get_bits(self):
        return self.__raw

    def solve(self):
        return self.answer.get_ip()
