from DNS.Connection import Connector, ServerConfiguration
from DNS.Requests.Request import DNSRequest, DNSResponse
from SocketContainer.SocketIO import SocketIO


class DNSClient:
    def __init__(self, server_config=None):
        if not isinstance(server_config, ServerConfiguration):
            raise TypeError
        self.server_config = server_config
        if not server_config:
            self.server_config = ServerConfiguration()
        self.__connector = Connector(
            address=self.server_config.get_address(),
            port=self.server_config.get_port(),
            server=False,
            tcp=False
        )
        self.io = SocketIO(sock=None, origin_address=self, debug=True)

    def set_up(self):
        self.__connector.connect()
        self.io.socket = self.__connector.get_socket()

    def sent_request(self, request):
        if not isinstance(request, DNSRequest):
            raise TypeError
        self.io.write(request.get_bits())

    def wait_for_response(self):
        my_bytes = self.io.read()
        response = DNSResponse()
        response.set_bits(my_bytes)
        return response
